#define keyboardChannel iChannel1
#define fontTexChannel iChannel2
#define bufAChannel iChannel3

// Thanks to FabriceNeyret for his help :-)

/*
Procedural modeling of galactic dust clouds (in arms, spurs, and at lower scales),
using a logarithmic spiral conform parametrization,
and multiplicative Perlin noise
Colors of stars and bulb are approximations - the main purpose of this work
is the dust clouds

Some controls are not on sliders - change the values in the define's below :-)

Controls :
G : display control sliders (see "## Sliders ##" below)
L : show the model in spiral parametric space
F : fancy colors / black & white
B : bulb
S : do the spurs
A : noise anisotropy
O : true opacity / raw density

(Debug) :
M : display the mesh
N : display the multiplicative noise
D : display info
I : invert the colors

## Sliders : ##

AW : Arms veins width
La : Lacunarity  @ not on point @

SA : Spurs incident angle with the arms
SF : Spurs frequency (= number of spurs)
SL : Spurs veins length
SW : Spurs veins width

DF : Veins displacement frequency
DA : Veins displacement amplitude

AA : Dust anisotropy angle
AS : Dust anisotropy strength

OF : Dust clouds layer opacity factor
LD : Low density factor (between veins)
HD : High density factor (within veins)
*/

//////////////////////////////////////////////
// Parameters for the spiral :
// Warp
#define BETA 1.38
// Rotation
#define GAMMA 0.38
// Number of spiral arms
#define N 2
// Direction of the arms
#define CLOCKWISE true
///////

//////////////////////////////////////////////
// Shape function for the spurs
// Both function and inverse derivative are needed (to match the spurs angle)
#if 1
    #define F(x) POW2(x)*2.
    // Derivative is 4x, so the inverse is x/4
    #define INV_DFDX(x) x/4.
#else
    #define F(x) exp(x)
    // Derivative is exp(x), so the inverse is log(x)
    #define INV_DFDX(x) log(x)
#endif

//////////////////////////////////////////////
// Colors settings
#define BULB_COLOR (FANCY_COL ? vec3(1,.95,.85) : BLACK)

#define DUST_COLOR (FANCY_COL ? vec3(.3,.15,.1)*1.2 : BLACK)
#define STARS_COLOR vec3(.85,.95,1)
#define HII_COLOR vec3(1,.2,.25)
#define BACKGROUND (FANCY_COL ? vec3(.25,.3,.3)*.5 : WHITE)





//////////////////////////////////////////////
// Slider i value (between 0 and 1), read from BufA

#define SliderValue(i) (texture(bufAChannel,SliderPosFromI(i)).x * sliders[i].value_factor)

#define ARM_WIDTH       	SliderValue(0)
#define LACUNARITY      	SliderValue(1)
#define SPURS_ANGLE     	SliderValue(2)
#define SPURS_FREQ      	SliderValue(3)
#define SPURS_LENGTH    	SliderValue(4)
#define SPURS_WIDTH     	SliderValue(5)
#define DISPLAC_FREQ    	(round(SliderValue(6)*FirstDiv*2.)/FirstDiv/2.)
#define DISPLAC_AMPL    	SliderValue(7)
#define ANISOTROPY_ANGLE   	SliderValue(8)
#define ANISOTROPY_STRENGTH	SliderValue(9)
#define DUST_OPACITY_FACTOR	SliderValue(10)
#define LOW_DENSITY_F   	SliderValue(11)
#define HIGH_DENSITY_F  	SliderValue(12)

// Cycle for the cyclic noise (will be =(n_orth,n))
vec2 cycle;


// Convert dust clouds density to an opacity
float densityToOpacity(float dens, float width, float opacity_factor){
    return 1. - exp(-dens*width*opacity_factor);
}


// High resolution density using Perlin multiplicative noise
float cloudDens(vec2 uv){
    
    // There are currently implementation problems about resolution
    // r should be the resolution for one cell of the mesh
    
    vec2 r = 1./fwidth(uv);
    return cyclicMultNoise(uv,r,cycle);
    
    //mat2 J = transpose(inverse(mat2(dFdx(uv),dFdy(uv))));
    //vec2 r = .5*vec2(length(J[0]),length(J[1]));
    //return cyclicMultNoise(uv,r,cycle);
    
    //return (.5+.5*sin(length(r))); // or r.x instead of length
    //return (.5+.5*sin(length(1./length(fwidth(uv)))));
}

//////////////////////////////////////////////
///////////////////  MAIN  ///////////////////
//////////////////////////////////////////////

void mainImage( out vec4 fragColor, vec2 fragCoord )
{
    vec3 color;
    
    // Centered normalized coordinates in [-1,1]
    vec2 pixel = 2.*(fragCoord-R.xy/2.) / R.y;
    
    // Perspective distortion to match M51
    pixel *= 1.1*vec2(.9,1);
    
    // Polar coordinates
	float rho = length(pixel),
          theta = atan(pixel.y,pixel.x); // theta in [-PI,PI]
    
    // Flip theta if the galaxy arms are clockwise
    if(CLOCKWISE)
       theta = -theta;
    
    // Parameters of the spiral
    float beta=BETA,    // warp
    	  gamma=GAMMA;  // rotation
    int   n=N;          // number of arms
    
    
    ///////////////////////
    // Conform parametrization along the logarithmic spiral
    // Idea : the orthogonal to a log spiral is another log spiral
    
    // Parameter for the orthogonal spirals
    float beta_orth = exp(-1./log(beta));
    
    // Number of arms in the orthogonal spiral
    int n_orth = int(round(float(n)*1./log(beta)));
    
    vec2 uv_param;
    if(LINEARIZE){
        // Use direct screen coordinates
        uv_param = fragCoord/R.y;
        uv_param.x += iMouse.x/R.x*10. - 1.;
        uv_param *= float(n);
    }
    else{
        // Continuous parameter along the spiral arms
        // uv_param.y is in [0,n[, orthogonal to arms
        // uv_param.x is in R, along the arms
        //     with 0 around the border of the screen (press M),
        //     and +inf at the center
        
        #define PARAM(BETA,N,GAMMA)\
                     (logBase(BETA,rho)-(theta+GAMMA))*float(N)/(2.*PI)
        uv_param.y = PARAM(beta     ,n     ,gamma);
        uv_param.x = PARAM(beta_orth,n_orth,0.   );
        // Move the discontinuity caused by atan (between -PI and PI)
        // st. parametrization is continuous along the arms
        uv_param.x += float(n_orth)*abs(floor(uv_param.y/float(n)));
        uv_param.y = mod(uv_param.y,float(n));
    }
    
    cycle = vec2(n_orth,n);
    
    ///////////////////////////////////
    
    /////////////////////////
	// Stars
    color = BACKGROUND;
    
    if(FANCY_COL){
        color = LINEARIZE ? color : mix(BULB_COLOR,color,clamp(rho,0.,1.));
        
        // Stars waves in arms
        float starfield = clamp(2.*periodicDirac(uv_param.y-ARM_POS-.15,1.,3.5)
                     * (POW2(exp(-cyclicMultNoise(uv_param,R.xy*5.,cycle)))*.2+.4) ,0.,1.);
        color = mix(color,
                    LINEARIZE ? STARS_COLOR : mix(BULB_COLOR,STARS_COLOR,clamp(rho,0.,1.)),
                    starfield);

        
        // Random stars
        color = mix(color,
                    STARS_COLOR,
                    smoothstep(.07,.06,cyclicMultNoise(fragCoord,R.xy,R.xy))
                   );
        

        // Red HII clusters
        vec4 hII = vec4(HII_COLOR,1)*
            pow(cyclicAddNoise(uv_param*2.5,6,cycle)*starfield,13.)*300.;
        color = mix(color,
                    hII.rgb,
                    min(hII.a,1.));
    }
    
    
    //////////////////////////
    // High resoluion dust clouds density
    float dens;
    if(NOISE_ANISOTROPY)
        dens = cloudDens(uv_param*RMat2D(ANISOTROPY_ANGLE)*vec2(1,1.+ANISOTROPY_STRENGTH));
    else
        dens = cloudDens(uv_param);
    
    
    //////////////////////////
    // Spiral arms veins
    
    // uv_param is original coordinates in parametrization
    // uv is a copy used for veins, for displacement
    vec2 uv = uv_param;
    
    if(DISPLAC_FREQ>0.)
    	uv += (cyclicAddNoise(uv*DISPLAC_FREQ,5,cycle*DISPLAC_FREQ)-.5)*DISPLAC_AMPL;
    
    // Periodic gaussian function in [0,1], period 1, spike at offset
    #define gauss(x,width) exp(-POW2((x)/(2.*width)))
    #define periodGauss(x,offset,width) gauss(mod(x+.5-offset,1.)-.5,width)
    float arms_veins = periodGauss(uv.y,ARM_POS,ARM_WIDTH / (LINEARIZE ? 1. : rho));
    
    
    //////////////////////////
    // Spurs veins
    float spurs_veins;
    if(DO_SPURS){
        float angle = PI/2.-SPURS_ANGLE;
        
        // Use a function for the shape of the spurs
        float y_spurs = mod(uv.y,1.)-ARM_POS;
        float fy = F(y_spurs + (INV_DFDX(tan(angle))));
        
        float u_spurs = (uv.x+fy)*(SPURS_FREQ);
        
        spurs_veins = periodGauss(u_spurs,0.,SPURS_WIDTH / (LINEARIZE ? 1. : rho));
        
        // Cut below the arm
        spurs_veins *= step(0.,y_spurs);
        
        // Smooth fading
        spurs_veins *= smoothstep(SPURS_LENGTH,0.,y_spurs);
        
    }
    else{
        spurs_veins = 0.;
    }
    
    
    //////////////////////////
    // Combine low resolution veins and high resolution density
    // to obtain final density
    
    // holes : low resolution lacunarity in veins
    // @ not on point, result is ugly @
    float holes = max(0.,-1. + (1.+LACUNARITY)*cyclicAddNoise(uv/FirstDiv*2.,5,vec2(n_orth,n)/FirstDiv*2.));
    
    // Combine veins by a max to avoid high value at incident points
    float veins = max(0.,max(arms_veins,spurs_veins) - holes);
    
    // Remap x from [0,1] to [a,b]
    #define remap(a,b,x) (x*(b-a)+a)
    
    // Combine veins and density
    float x = dens * remap(LOW_DENSITY_F,HIGH_DENSITY_F,veins);
    
    // Get opacity from density
    if(!RAW_DENSITY)
        x = densityToOpacity(x,1.,DUST_OPACITY_FACTOR);
    
    ///////////////
    //## Debug ##//
    //x = dens;
    //x = spike;
    //x = fwidth(spike)*10.;
    //x = holes;
    //x = fwidth(uv.y);
    //x = uv_param.y/2.;
    //x = POW3(uv_param.x/25.);
    ///////////////
    
    // Use dust clouds opacity to hide stars light
    color = mix(color,DUST_COLOR,x);
    
    // Bulb
    if(DO_BULB && !LINEARIZE){
        color = mix(color,BULB_COLOR,clamp(1.-pow(rho,BULB_COMPR)/BULB_R,0.,1.)*.9);
    }
    
    //////////////////////////////////////
    //////////////////////////////////////
    //////// Additionnal displays ////////
    //////////////////////////////////////
    //////////////////////////////////////
    
    // Clamp the color st. blending with additionnal displays stays nice
    color = clamp(color,0.,1.);
    
    /////////////////////////
    // Draw the mesh
    if(DISP_MESH){
        float s = float(1 << MESH_COMPR);
        if(!LINEARIZE)
            s *= rho*sqrt(rho)*log(beta);
        #define DRAWMESH(XY) color = mix(color,GREEN/2.,periodicDirac(uv_param.XY,1.,s))
        DRAWMESH(x);
        DRAWMESH(y);
        // Draw the 0 line on x (pink = +, turquoise = -)
    	if(abs(uv_param.x)<.015) color = uv_param.x>0. ? PINK : TURQUOISE;
    }
    
    
    // Display only multiplicative noise
    if(DISP_NOISE){
        vec2 mouse = 2.*iMouse.xy/R.y;
        float dens = cloudDens(pixel-mouse);
        color = (RAW_DENSITY ?
                 vec3(.5,.2,.2)*dens :
                 2.*vec3(.5,.7,.9)*(1.-densityToOpacity(dens,1.,DUST_OPACITY_FACTOR))
                 );
    }
    
    #define tex fontTexChannel

    // Shortcuts for printing
    #define NextLine nextLine(pp)
    #define WriteWord(word,style) writeWord(word,style,pp,tex,color)
    #define WriteNumber(number,d1,d2,style) writeNumber(number,d1,d2,style,pp,tex,color)
    #define WriteStandardChar(char,style) writeStandardChar(char,style,pp,tex,color)

    
    // Display information about opacity/density
    if(DISP_INFO || DISP_NOISE){
        
        vec2 pos = vec2(10,335)/vec2(640,360);
        float scale = 1./30.;
        PrintPosition pp = getDisplayPos(pos,scale);
        PrintStyle style = DefaultPrintStyle;
        WriteWord(int[](_D,_i,_s,_p,_l,_a,_y,_COLON,_),style);
        if(RAW_DENSITY){
            int[] word_opacity = int[](_D,_e,_n,_s,_i,_t,_y,_,
                                       _LPAR,_r,_a,_w,_,_n,_o,_i,_s,_e,_RPAR);
            WriteWord(word_opacity,style);
        }
        else{
            int[] disp_width = int[](_O,_p,_a,_c,_i,_t,_y,_,
                                     _MINUS,_,_D,_u,_s,_t,_,_o,_p,_a,_c,_i,_t,_y,_,_f,_a,_c,_t,_o,_r,_,_EQ,_);
            WriteWord(disp_width,style);
            WriteNumber(DUST_OPACITY_FACTOR,1,2,style);
        }
    }
    
    // Display the sliders
    if(DISP_SLIDERS){
        float slider_disp = texelFetch(bufAChannel,ivec2(fragCoord),0).y;
        
    	color = mix(color, SLIDER_COLOR, slider_disp);
        vec2 pos = vec2(5,106)/vec2(640,360);
        float scale = 1./29.9;
        PrintPosition pp = getDisplayPos(pos,scale);
        PrintStyle style1 = DefaultPrintStyle;
        
        // Write sliders labels
        WriteWord(sliders_labels,style1);
        
        // Write sliders values
        PrintStyle style2 = NewPrintStyle(BLACK,GREEN);
        pos = vec2(6,17)/vec2(640,360);
        scale = 1./34.;
        pp = getDisplayPos(pos,scale);
        for(int i=0; i<NB_SLIDERS; i++){
            WriteNumber(min(SliderValue(i),9.9),1,1,style2);
            WriteStandardChar(_,style2);
        }
        
        
        int islider = int(texture(bufAChannel,vec2(0)).z*float(NB_SLIDERS));
        
        if(islider < NB_SLIDERS){
            // Write current slider full name
        	PrintStyle style4 = NewPrintStyle(BLACK,LAGOON);
            pos = vec2(max(float(islider)*(SLIDER_W+SLIDER_SPACING)*640.-50.,5.),122)/vec2(640,360);
        	scale = 1./30.;
       		pp = getDisplayPos(pos,scale);
            int[19] name;
            switch(islider){
                case 0 : name = int[](_A,_r,_m,_s,_ ,_w,_i,_d,_t,_h,_ ,_ ,_ ,_ ,_ ,_ ,_ ,_ ,_ ); break;
                case 1 : name = int[](_L,_a,_c,_u,_n,_a,_r,_i,_t,_y,_ ,_ ,_ ,_ ,_ ,_ ,_ ,_ ,_ ); break;
                case 2 : name = int[](_S,_p,_u,_r,_s,_ ,_a,_n,_g,_l,_e,_ ,_ ,_ ,_ ,_ ,_ ,_ ,_ ); break;
                case 3 : name = int[](_S,_p,_u,_r,_s,_ ,_f,_r,_e,_q,_u,_e,_n,_c,_y,_ ,_ ,_ ,_ ); break;
                case 4 : name = int[](_S,_p,_u,_r,_s,_ ,_l,_e,_n,_g,_t,_h,_ ,_ ,_ ,_ ,_ ,_ ,_ ); break;
                case 5 : name = int[](_S,_p,_u,_r,_s,_ ,_w,_i,_d,_t,_h,_ ,_ ,_ ,_ ,_ ,_ ,_ ,_ ); break;
                case 6 : name = int[](_D,_i,_s,_p,_l,_a,_c,46,_ ,_f,_r,_e,_q,_u,_e,_n,_c,_y,_ ); break;
                case 7 : name = int[](_D,_i,_s,_p,_l,_a,_c,46,_ ,_a,_m,_p,_l,_i,_t,_u,_d,_e,_ ); break;
                case 8 : name = int[](_A,_n,_i,_s,_o,_t,_r,_o,_p,_y,_ ,_a,_n,_g,_l,_e,_ ,_ ,_ ); break;
                case 9 : name = int[](_A,_n,_i,_s,_o,_t,_r,_o,_p,_y,_ ,_s,_t,_r,_e,_n,_g,_t,_h); break;
                case 10: name = int[](_D,_u,_s,_t,_ ,_o,_p,_a,_c,_i,_t,_y,_ ,_f,_a,_c,_t,_o,_r); break;
                case 11: name = int[](_L,_o,_w,_ ,_d,_e,_n,_s,_i,_t,_y,_ ,_f,_a,_c,_t,_o,_r,_ ); break;
                case 12: name = int[](_H,_i,_g,_h,_ ,_d,_e,_n,_s,_i,_t,_y,_ ,_f,_a,_c,_t,_o,_r); break;
            }
            WriteWord(name,style4);
        }
        
        // Global restore default button
        PrintStyle style3 = NewPrintStyle(BLACK,LIGHT_BLUE*2.);
        pos = vec2(560,20)/vec2(640,360);
        scale = 1./30.;
        pp = getDisplayPos(pos,scale);
        WriteWord(int[](_R,_e,_s,_t,_o,_r,_e),style3);
        NextLine;
        WriteWord(int[](_d,_e,_f,_a,_u,_l,_t),style3);
        
        // Per-slider restore default buttons
        pos = vec2(10,-6)/vec2(640,360);
        scale = 1./20.;
        pp = getDisplayPos(pos,scale);
        for(int i=0; i<NB_SLIDERS; i++){
            WriteStandardChar(26,style3);
            WriteStandardChar(_,style3);
        }
    }
    
    if(INVERT_COLORS)
    	color = 1.-color;
    
    fragColor = vec4(color,1);
    
}
