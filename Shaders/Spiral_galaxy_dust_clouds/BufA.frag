
#define selfChannel iChannel0
#define keyboardChannel iChannel1

/*
    Utils buffer for sliders :
- o.x is sliders value
- o.y is sliders display
- o.z is slider currently being clicked (to display corresponding text)
- o.w is previous R.x (to reload sliders upon window resizing)
*/

// Draw a slider as 2 rectangles
float dispSlider(vec2 uv, vec2 pos, float value){
    return step(0., - dRect(uv,pos,vec2(SLIDER_LINE_W*R.y/R.x,SLIDER_H))
               		* dRect(uv,pos+vec2(0,value*SLIDER_H-SLIDER_H/2.),vec2(SLIDER_W,SLIDER_LINE_W)));
}

void mainImage( out vec4 o, in vec2 fragCoord )
{
    vec2 uv = fragCoord/R.xy,
         mouse = iMouse.xy/R.xy,
         click = iMouse.zw/R.xy;
    vec4 prev_o = texture(iChannel0,uv);
    
    bool resized = prev_o.w != R.x/1200.;
    
    if((!DISP_SLIDERS || click.x<0.) && !resized){
        // Nothing to do, o is previous o
        o = prev_o;
        o.z = 1.;
        return;
    }
    
    // General restore defaults button
    bool restore_defaults = DISP_SLIDERS && click.x>.87 && click.y<.11;
    
    // Slider of the current pixel
    int islider_uv = iSliderFromPos(uv);
    
    // Init / Re-init at default values
    if(iFrame == 0 || resized || restore_defaults){
        float value = sliders[islider_uv].def_value;
        float display = islider_uv<NB_SLIDERS ?
                               dispSlider(uv,SliderPosFromI(islider_uv),value)
                               : 0.;
        o = vec4(value,display,1.,R.x/1200.);
        return;
    }
    
    // Current slider according to mouse click x, between 0 and NB_SLIDERS-1
    // (or more if mouse beyond sliders)
    int islider_mouse = iSliderFromPos(click);
    
    // Was the mouse click on the slider ?
    bool mouse_x_in_slider = mod(click.x-SLIDER_POS.x+SLIDER_W/2.,SLIDER_W+SLIDER_SPACING)<=SLIDER_W;
    bool mouse_y_in_slider = abs(click.y-SLIDER_POS.y) < SLIDER_H/2.;
    bool mouse_in_slider = mouse_x_in_slider && mouse_y_in_slider;
    
    // Current slider in o.z
    o.z = mouse_in_slider ? float(islider_mouse)/float(NB_SLIDERS) : 1.;
    
    // Is the current pixel on the same slider than the mouse click ?
    bool pixel_in_mouse_slider = (islider_uv == islider_mouse);
    
    if(!pixel_in_mouse_slider || islider_mouse>=NB_SLIDERS){
        // Nothing to do, o is previous o
        // but still update o.z
        o.xyw = prev_o.xyw;
        return;
    }
    
    // Current slider restore default button
    bool restore_default_current = click.y<=.055 && mouse_x_in_slider;
    
    // Current slider update
    if(mouse_in_slider || restore_default_current){
        float value = restore_default_current ?
            				  sliders[islider_mouse].def_value
            				: clamp((mouse.y-SLIDER_POS.y+SLIDER_H/2.)/SLIDER_H,0.,1.);
        float display = dispSlider(uv,SliderPosFromI(islider_mouse),value);
        o = vec4(value,display,o.z,R.x/1200.);
    }
    else{
        o = prev_o;
    }
}
