
#define selfChannel iChannel0
#define keyboardChannel iChannel1

#define keyToggleRaw(K) (texture(keyboardChannel,vec2((.5 + float(K))/256.,0.75)).x > 0.)
#define keyPressRaw(K) (texelFetch(keyboardChannel, ivec2(K, 0), 0).x > 0.)

/*
    Utils buffer :
- o.x is keyToggle
- o.y is keyPressed
- o.zw is mouse.xy when F was toggled (for ruler)
*/
void mainImage( out vec4 o, vec2 fragCoord )
{
    float u = fragCoord.x/R.x;
    bool key_toggled = keyToggleRaw(u*256.);
    bool key_pressed = keyPressRaw(u*256.);
    bool f_toggled = keyToggleRaw(_F);
    o = vec4(key_toggled,
             key_pressed,
             f_toggled ? texture(selfChannel,vec2(0)).zw : iMouse.xy/R.y
            );
}
