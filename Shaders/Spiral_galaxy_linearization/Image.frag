
#define galaxyTexChannel iChannel0
#define bufAChannel iChannel1
#define bufBChannel iChannel2
#define fontTexChannel iChannel3

/*/
/*
/* This shader purpose is to linearize spirals to observe galaxy dust clouds,
/* in a research context. No particular aestheticism intended :)
/*
/*/

/*
Galaxy textures (set GALAXY in Common)

Galaxy textures can be obtained by add-on on browser for custom textures :
Firfox : https://addons.mozilla.org/en-US/firefox/addon/shadertoy-custom-texures/
Chrome : https://chrome.google.com/webstore/detail/shadertoy-custom-texures/jgeibpcndpjboeebilehgbpkopkgkjda

or by using js console (press F12) :

######
# M51

# Low Res :
gShaderToy.SetTexture(0, {mSrc:'https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Messier51_sRGB.jpg/1280px-Messier51_sRGB.jpg', mType:'texture', mID:1, mSampler:{ filter: 'mipmap', wrap: 'clamp', vflip:'true', srgb:'false', internal:'byte' }});

# High Res :

gShaderToy.SetTexture(0, {mSrc:'https://upload.wikimedia.org/wikipedia/commons/d/db/Messier51_sRGB.jpg', mType:'texture', mID:1, mSampler:{ filter: 'mipmap', wrap: 'clamp', vflip:'true', srgb:'false', internal:'byte' }});

#
######

######
# M101

# Low Res :

gShaderToy.SetTexture(0, {mSrc:'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/M101_hires_STScI-PRC2006-10a.jpg/1280px-M101_hires_STScI-PRC2006-10a.jpg', mType:'texture', mID:1, mSampler:{ filter: 'mipmap', wrap: 'clamp', vflip:'true', srgb:'false', internal:'byte' }});

# High Res :

gShaderToy.SetTexture(0, {mSrc:'https://upload.wikimedia.org/wikipedia/commons/c/c5/M101_hires_STScI-PRC2006-10a.jpg', mType:'texture', mID:1, mSampler:{ filter: 'mipmap', wrap: 'clamp', vflip:'true', srgb:'false', internal:'byte' }});

#
######
*/

#define keyState(K) texture(bufBChannel,vec2(float(K)/256.))
#define keyToggle(K) (keyState(K).x > .5)
#define keyPress(K) (keyState(K).y > .5)
#define frozenMousePos (texture(bufBChannel,vec2(0)).zw)

/////////////////////////////////////////////////////
// Keyboard controls

// 'L' : switch display : normal / linearized
#define LINEARIZE			 keyToggle(_L)
// 'Z' : zoom
#define ZOOM				!keyToggle(_Z)
#define ZOOM_FACTOR	(ZOOM ? 1. : 3.)
// 'F' : freeze image, display ruler
#define DISP_RULER			 keyToggle(_F)
// 'R' : switch ruler type
#define CUSTOM_RULER_MODE	!keyToggle(_R)
// 'T' : display thumbnail
#define DISP_THUMBNAIL		!keyToggle(_T)
// 'I' : invert colors
#define INVERT_COLORS		 keyToggle(_I)

// Debug :
// 'D' : debug display
#define DISP_PARAMS			 keyToggle(_D)
// 'M' : toggle mesh
#define DRAW_MESH			!keyToggle(_M)


// Contrast increase (0 : no contrast increase)
#define CONTRAST 1
#define INCR_CONTRAST(COL) smoothstep(0.,1.,COL)


// Access the texture of the galaxy at galaxyTexChannel
// Distort it according to GALAXY_DISTORTION
// Result a-channel is set to 0 if out of texture, 1 otherwise
vec4 galaxyTexture(vec2 uv){
    
    // Cut if original uv is not in [0,1]
    bool out_of_tex = abs(uv.x-.5)>.5 || abs(uv.y-.5)>.5;
    
    // Spiral mesh
    vec4 mesh = texture(bufAChannel,uv);
    
    // Galaxy photo distortion to get a nice, not distorted spiral
    vec2 screen_ratio = vec2((R.x/R.y)/(640./360.),1);
    vec2 img_compr = GALAXY_DISTORTION*screen_ratio;
    uv = (uv-.5)*img_compr + GALAXY_CENTER;
    
    vec3 color = mix(texture(galaxyTexChannel,uv).rgb,
                     mesh.rgb,
                     DRAW_MESH ? mesh.a : 0.);
    
    out_of_tex = out_of_tex
          || abs(uv.x-.5)>.5 || abs(uv.y-.5)>.5 // Cut if new uv is not in [0,1]
          || (GALAXY==M51 && uv.x>.700); // Cut the satellite galaxy on M51
    
    return vec4(color, !out_of_tex);
}

//////////////////////////////////////////////
///////////////////  MAIN  ///////////////////
//////////////////////////////////////////////

void mainImage( out vec4 fragColor, vec2 fragCoord )
{
    vec3 color = BLACK;
    
    // Normalized coordinates in [0,1]
    vec2 pixel = fragCoord/R.xy;
    
    // Parameters of the spiral
    float beta=BETA,    // warp
    	  gamma=GAMMA;  // rotation
    int   n=N_GALAXY;   // number of arms

    ///////////////////////
    // Parameter for the orthogonal spirals
    float beta_orth = exp(-1./log(beta));
    
    // Number of spirals in the orthogonal mesh
    int n_orth = int(round(float(n)*1./log(beta)));
    
    vec2 uv_tex;
    
    if(LINEARIZE){
       // Linearize the texture in the logarithmic spiral space
       // => Find the inverse parametrization to access the texture
        vec2 mouse_offset = 
            (DISP_RULER ? frozenMousePos : iMouse.xy/R.y)*(ZOOM ? 2. : 1.)
             + vec2(0,-.7)*(ZOOM ? 4. : 1.);
        
        vec2 xy = (fragCoord/R.y - mouse_offset) *float(n)*ZOOM_FACTOR;
        
        float theta = (2.*PI)/float(n_orth) * (xy.x*log(beta_orth) - xy.y)
                      /(log(beta/beta_orth));
        float rho = pow(beta,theta+2.*PI*xy.y/float(n));
        if(CLOCKWISE)
            theta = -theta;
        
        uv_tex = polToCart(rho,theta);
        uv_tex = uv_tex/R.xy+.5;
    }
    else{
        // Access directly the texture
        uv_tex = pixel;
    }
    
    // Retrieve the galaxy texture
    vec4 galaxy_tex = galaxyTexture(uv_tex);
    
    // Display grey outside of the texture
    color = mix(GREY(.5),galaxy_tex.rgb,galaxy_tex.a);
    
    // Thumbnail
    if(LINEARIZE && DISP_THUMBNAIL){
        float thb_size = .42;
        vec2 thb_pos = vec2(-.8,.7);
        vec2 uv_tex_thb = (pixel-.5)/thb_size+.5 - thb_pos;
        vec4 thumbnail = galaxyTexture(uv_tex_thb);
        color = mix(color,thumbnail.rgb,thumbnail.a);
    }
    
    // Increase contrast
    for(int i=0; i<CONTRAST; i++)
    	color = INCR_CONTRAST(color);
    
    
    
    //////////////////////////////////////
    //////////////////////////////////////
    //////// Additionnal displays ////////
    //////////////////////////////////////
    //////////////////////////////////////
    
    #define tex fontTexChannel
    
    // Shortcuts for printing
    #define NextLine nextLine(pp)
    #define WriteWord(word,style) writeWord(word,style,pp,tex,color)
    #define WriteNumber(number,d1,d2,style) writeNumber(number,d1,d2,style,pp,tex,color)
    #define WriteStandardChar(char,style) writeStandardChar(char,style,pp,tex,color)
    
    PrintStyle style = DefaultPrintStyle;
    
    // Rulers
    if(DISP_RULER){
        
        #define S(a) smoothstep(1.,-1.,a)
        
        vec3 ruler_color = WHITE;
        
        // Dynamic ruler by click & drag
        // Display distance and angle
        if(CUSTOM_RULER_MODE){
            
            vec2 pos = vec2(400,335)/vec2(640,360);
            float scale = 1./30.;
            PrintPosition pp = getDisplayPos(pos,scale);
            
            WriteWord(int[](_DELTA,_EQ),style);
            
            vec2 p = fragCoord;  // Current pixel
            vec2 p1 = iMouse.zw; // Mouse click position
            vec2 p2 = iMouse.xy; // Mouse drag position
            float dist_p_p1 = length(p - p1);
            float dist_p_p2 = length(p - p2);
            float dist_p1_p2 = length(p1 - p2);
            
            #define DRAW_SEG(P,P1,P2,WIDTH)\
                        color = mix(color,ruler_color,\
                                    S(distPointToSegment(P,P1,P2)-WIDTH));
            
            bool mouse_pressed = (p1.x >= 0.);
            
            if(mouse_pressed){
                // Mouse is pressed, draw the segment between p1 and p2
                DRAW_SEG(p,p1,p2,1.);
                // Display the distance
                WriteNumber(dist_p1_p2/R.y*100.*ZOOM_FACTOR,1,1,style);
        	}
            
            NextLine;
            WriteWord(int[](_theta,_EQ),style);
            
            if(mouse_pressed){
                // Mouse is pressed, draw the angle
                
                // Absolute angle in [0,PI/2.] between (P1,P2) and the x-axis
                #define ANGLE(P1,P2,DIST)\
                           (DIST==0. ? 0. : atan(abs(P1.y-P2.y),abs(P1.x-P2.x)))
                
                float angle_p1_p2 = ANGLE(p1,p2,dist_p1_p2);
                
                // Angle draw size
                float angle_size = .06*R.y;
                if(dist_p1_p2>angle_size){
                    // The distance is long enough to draw the angle
                    float line_width = .6;
                    vec2 p2_side = sign(p2-p1);
                    vec2 p_side = sign(p-p1);
                    
                    // Draw the horizontal segment
                    DRAW_SEG(p,
                              p1+vec2(angle_size*max(p2_side.x-1.,-1.),0),
                              p1+vec2(angle_size*min(p2_side.x+1.,1.),0),
                              line_width);
                    
                    // Draw the angle curve
                    float angle_p1_p = ANGLE(p1,p,dist_p_p1);
                    if(angle_p1_p<=angle_p1_p2 && p2_side == p_side)
                        color = mix(color,ruler_color,
                                    S(abs(dist_p_p1-angle_size*.8)-line_width));
                }
                
                // Display the angle value
                WriteNumber(degrees(angle_p1_p2),1,1,style);
        		WriteStandardChar(_DEG,style);
                
                // Draw the dots at p1 and p2
                #define DRAW_DOT(C,D)\
                            color = mix(color,C,S(D-4.));
                DRAW_DOT(TURQUOISE,dist_p_p1);
                DRAW_DOT(LAGOON,dist_p_p2);
            }
        }
        // Static ruler, along x and y axis
        else{
            vec2 p = fragCoord - iMouse.xy; // mouse-centered coords
            vec2 dist_p_axis = abs(p.yx);
            #define DRAW_RULER(XY)\
                    if(dist_p_axis.XY< (mod(p.XY,.1*R.y/ZOOM_FACTOR)<1. ? 5. : 1.))\
                         color = ruler_color;
            DRAW_RULER(x);
            DRAW_RULER(y);
        }
    }
    
    
    // Debug display (beta & gamma & n)
    if(DISP_PARAMS){
        
        vec2 pos = vec2(320,335)/vec2(640,360);
        float scale = 1./30.;
        PrintPosition pp = getDisplayPos(pos,scale);

        WriteWord(int[](_beta,_EQ),style);
        WriteNumber(beta,1,2,style);
        NextLine;

        WriteWord(int[](_gamma,_EQ),style);
        WriteNumber(gamma,1,2,style);
        NextLine;

        WriteWord(int[](_n,_EQ),style);
        WriteNumber(float(n),1,0,style);
    }
    
    /* Calibrating tool *
    if(any(lessThan(abs(fragCoord-R.xy/2.),vec2(1))))
        color = WHITE;
    /**/
    
    if(INVERT_COLORS)
    	color = 1.-color;
    
    fragColor = vec4(color,1);
    
}
