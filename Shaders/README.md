# Shaders

## Final shaders :
- **Dust clouds modelisation** : [Spiral galaxy dust clouds](Spiral_galaxy_dust_clouds) (or [on shadertoy](https://www.shadertoy.com/view/lldczr))  
  Procedural modeling of galactic dust clouds (in arms, spurs, and at lower scales), using a logarithmic spiral conform parametrization, and multiplicative Perlin noise.
  Our approach results in a parametrable model (there are nice sliders in the shader !).
- **Galaxy linearization** : [Spiral galaxy linearization](Spiral_galaxy_linearization) (or [on shadertoy](https://www.shadertoy.com/view/lsKcWc))  
  "Linearization" of spiral galaxies in the logarithmic spiral parametric space. The goal is to analyze dust clouds in this space.
  Requires custom textures add-ons ([Chrome](https://chrome.google.com/webstore/detail/shadertoy-custom-texures/jgeibpcndpjboeebilehgbpkopkgkjda) or [Firefox](https://addons.mozilla.org/en-US/firefox/addon/shadertoy-custom-texures/)) to work on Shadertoy.
  See the [pictures](https://gitlab.com/Astor-Bizard/Galaxy_Pictures) for which presets are written.


## Research steps :
- Find orthogonal parametrization to a spiral :
  - With strange spiral :
    - [Spiral distortion on shadertoy](https://shadertoy.com/view/XsfSzN) : an old attempt to make orthognal spiral mesh on strange Archimede spirals
    - [Spiral tangents&normals on shadertoy](https://www.shadertoy.com/view/lddcRs) : determining normals to the strange Archimede spirals
  - With logaritmic spiral :
    - [Spiral orthogonality on shadertoy](https://www.shadertoy.com/view/Xd3cWf) : determining normals to the logartihmic spiral, and getting the idea that the orthogonal curve to a log spiral is another log spiral
    - [Spiral regular orthogonal mesh on shadertoy](https://www.shadertoy.com/view/lsKyz1) : determining the orthonormal log spiral and conform number of arms
    - [Spiral orth parametrization on shadertoy](https://www.shadertoy.com/view/XdyyWw) : determining a conform parametrization of the log spiral
- Observation of dust clouds in parametric space :
  - **[Spiral galaxy linearization](Spiral_galaxy_linearization) (see above)**
- Dust clouds modelisation using [multiplicative Perlin noise](https://www.shadertoy.com/view/Xs23D3) :
  - [Noise experimentations on shadertoy](https://www.shadertoy.com/view/MddfRM) : studying various methods for the modelisation in veins
  - [Spiral galaxy noisy arms on shadertoy, versions 1](https://www.shadertoy.com/view/ldVcDm),[2](https://www.shadertoy.com/view/Xd3fzl),[3](https://www.shadertoy.com/view/ld3fWB)
  - **[Spiral galaxy dust clouds](Spiral_galaxy_dust_clouds) (see above)**

## Implementation limitations
In the clouds modelisation, there are some implementation problems :
- *Spurs* are not managed to be continuous on the parametrization discontinuity
- Anisotropic cyclic noise is not continuous (isotropic cyclic noise is)
- Multiplicative Perlin noise mapped on the logarithmic parametrization has resolution/aliasing problems.

## Backup file
This is a backup of shaders on Shadertoy. Its purpose is really to back up in case of Shadertoy database crashing. It includes shaders unrelated to this project, no sorting has been done.