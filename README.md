# Procedural modeling of multi-scale galactic dust
Astor Bizard  
M2 MoSIG Research project (2018)  
Performed at Maverick Team (LJK, Inria)  
Under the supervision of Fabrice Neyret, Joelle Thollot, Romain Vergne

## Sources
The source code is written as fragment shaders for the [Shadertoy](http://www.shadertoy.com) API.  
It requires a web browser supporting WebGL 2.0.
These can be adapted for standard C++/GLSL by using [libshadertoy](https://gitlab.inria.fr/vtaverni/libshadertoy).

Sources are available in Shadertoy [here](https://www.shadertoy.com/user/ABizard).

## Pictures
Galaxies large pictures are stored on [this repository](https://gitlab.com/Astor-Bizard/Galaxy_Pictures)