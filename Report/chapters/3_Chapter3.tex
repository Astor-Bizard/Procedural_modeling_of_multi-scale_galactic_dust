\chapter{\label{chap:contrib}Paramétrisation de spirale logarithmique et application du bruit de Perlin multiplicatif}

Lors de la modélisation de surfaces, une étape nécessaire au plaquage de textures ou d'autres éléments paramétrés (comme un bruit procédural) est de déterminer une paramétrisation de cette surface. Une paramétrisation consiste à transposer des coordonnées d'un espace à un autre : dans le cas du plaquage d'une texture, il s'agit de transposer les coordonnées d'un espace cartésien à deux dimensions (l'espace de la texture) vers l'espace de la surface. De plus, si nous voulons appliquer de tels éléments sans introduire de distorsion, il est nécessaire de déterminer une paramétrisation isométrique (conservant les longueurs) et conforme (conservant les angles).

Nous proposons dans la Section \ref{sec:parametrisation} une approche de paramétrisation conforme de spirale multi-bras 2D. Notre paramétrisation offre une bijection entre l'espace cartésien ou polaire (cf Fig \ref{fig:pol}) et l'espace de la spirale (espace présentant une dimension le long des bras spiraux et l'autre dimension orthogonale aux bras).
La paramétrisation obtenue nous permet de modéliser des nuages de poussière galactiques à différentes échelles (Section \ref{sec:modelisation}), en proposant des paramètres réglables dans un espace adapté.

\section{\label{sec:parametrisation}Paramétrisation conforme de la spirale logarithmique}

Les galaxies spirales les plus régulières prennent la forme de spirales qui s'approchent de spirales logarithmiques (ce que confirment nos observations, cf Fig. \ref{fig:warps} et la littérature, cf \cite{elmegreen1989spiral}). Nous nous concentrons sur ce modèle, qui pourra être généralisé à d'autres formes de spirales, pour d'autres galaxies\footnote{Il est également possible que certaines galaxies ne suivent pas un unique modèle de spirale, ce dernier pouvant varier avec la distance au centre de la galaxie, notamment près du bulbe (à fortiori pour les galaxies spirales barrées) ou vers l'extérieur de la galaxie.}.

Une spirale logarithmique est de la forme : $$\rho = \alpha.\beta^\theta$$ où $(\rho,\theta)$ sont les coordonnées polaires (cf Fig \ref{fig:pol}), $\alpha$ représente un zoom (équivalent à une rotation pour les spirales logarithmiques) et $\beta$ représente le \textit{warping} (la distorsion) de la spirale.\\
\textbf{Note :} Nous fixons $\alpha=1$ pour simplifier les formules, qui restent généralisables à une rotation près.

Les galaxies spirales à plusieurs bras sont souvent relativement régulières (c'est-à-dire que leurs bras sont régulièrement espacés). Dans ce cas, la formule pour chaque bras $i$ d'une spirale régulière à $n$ bras est : $$\rho = \beta^{\theta + i\frac{2\pi}{n}} \mathrm{\ ,\ avec\ } i\in\{0\dots n-1\}$$

\begin{figure}
\centering
\includegraphics[width=.3\textwidth]{pics/coords_polaires.jpg}
\caption{\label{fig:pol}Un point de coordonnées $(x,y)$ dans un repère cartésien peut être représenté dans un repère polaire par les coordonnées $(\rho,\theta)$. Nous avons les relations $x = \rho\cos\theta$, $y = \rho\sin\theta$, $\rho = \|(x,y)\|$, $\theta = \arctan(\frac{y}{x})$.}
\end{figure}

\begin{figure*}
\centering
 \begin{subfigure}[t]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{pics/M51_warp_3.png}
 \caption{\label{fig:m51warp}Les bras de la galaxie M51 suivent une spirale logarithmique \cite{elmegreen1989spiral} régulière à deux bras pour $\beta=1.38$ (nous avons corrigé la perspective de cette image pour effectuer la correspondance de spirale). Nous remarquons cependant qu'à partir d'un certain rayon, les bras s'écartent de ce modèle. Cela est dû notamment à la galaxie satellite qui déforme M51 \cite{dobbs2010simulations} (visible à droite de l'image).}
 \end{subfigure}%
 ~
 \hspace{0.05\textwidth}
 \begin{subfigure}[t]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{pics/MilkyWay_warp2.png}
 \caption{\label{fig:milkywaywarp}Nous observons que sur la reconstitution de la Voie Lactée de la NASA \cite{milkyway_carto}, ses bras suivent également une spirale logarithmique régulière à deux bras\protect\footnotemark, pour $\beta=1.25$. De plus, nous pouvons constater que le cœur en forme de barre de la galaxie n'altère pas le modèle de spirale, bien que celle-ci n'apparaisse pas dans le bulbe.}
 \end{subfigure}
 \caption{\label{fig:warps}Justification du choix du modèle de spirale logarithmique pour modéliser les galaxies spirales.}
\end{figure*}
\footnotetext{Il faut garder à l'esprit que cette reconstitution a peut-être été construite arbitrairement selon ce modèle de spirale.}

Nous nous intéressons dans un premier temps à obtenir une paramétrisation orthogonale.
Pour obtenir une paramétrisation orthogonale, nous avons besoin de la formule de la courbe orthogonale à la spirale. Pour cela, nous devons déterminer la normale à la spirale, à partir de la tangente.
Dans l'espace polaire, la tangente à une spirale logarithmique de paramètre $\beta$ est  :
\begin{align*}
\frac{d\rho}{d\theta}(\beta) &= \beta^{\theta + 2\frac{i}{n}\pi}.\ln\beta.\frac{d(\theta + 2\frac{i}{n}\pi)}{d\theta}\\
&= \rho.\ln\beta
\end{align*}

L'espace polaire n'offre pas la conservation des propriétés d'orthogonalité du repère cartésien (dont nous avons besoin afin d'obtenir la normale à partir de la tangente). Afin d'obtenir ces propriétés d'orthogonalité, nous avons besoin de transposer la tangente en coordonnées cartésiennes.
Nous avons :
\begin{align*}
&&x&=\rho\cos\theta &y&=\rho\sin\theta\\
&&\Rightarrow\frac{dx}{d\theta}&=\frac{d\rho}{d\theta}\cos\theta - \rho\sin\theta &\Rightarrow\frac{dy}{d\theta}&=\frac{d\rho}{d\theta}\sin\theta + \rho\cos\theta
\end{align*}

Ainsi, dans l'espace cartésien, la tangente à une spirale de paramètre $\beta$ en $\theta$ est :
\begin{align*}
\frac{dy}{dx}(\beta) &= \frac{\frac{d\rho}{d\theta}(\beta)\sin\theta + \rho\cos\theta}{\frac{d\rho}{d\theta}(\beta)\cos\theta - \rho\sin\theta}\\
&= \frac{\rho\ln\beta\sin\theta + \rho\cos\theta}{\rho\ln\beta\cos\theta - \rho\sin\theta}\\
&= \frac{\rho.(\ln\beta\sin\theta + \cos\theta)}{\rho.(\ln\beta\cos\theta - \sin\theta)}\\
&= \frac{\ln\beta\sin\theta + \cos\theta}{\ln\beta\cos\theta - \sin\theta}
\end{align*}
%L'équation de la tangente à la spirale en $\theta$ est $$y - \rho\sin\theta = \frac{dy}{dx}.(x-\rho\cos\theta)$$ et la normale à la spirale au même point est $$y - \rho\sin\theta = \frac{-1}{\frac{dy}{dx}}.(x-\rho\cos\theta)$$ $$\frac{dy}{dx}$$
et la normale à la spirale au même point est $$\frac{-1}{\frac{dy}{dx}(\beta)}$$

Nous voulons trouver la courbe orthogonale à la spirale, donc la courbe interpolante des normales.
\textbf{Hypothèse :} cette courbe est une autre spirale logarithmique, de paramètre $\beta'$.
Dans ce cas, la normale d'une spirale en $\theta$ est la tangente de l'autre spirale au même point :
%\begin{align*} 
%&&y - \rho\sin\theta = \frac{dy}{dx}.(x-\rho\cos\theta) &\equiv y - \rho\sin\theta = \frac{-1}{\frac{dy}{dx}'}.(x-\rho\cos\theta)\\
%&\Leftrightarrow& \frac{dy}{dx} &= \frac{-1}{\frac{dy}{dx}'}\\
\begin{align*}
&&\frac{dy}{dx}(\beta) &= \frac{-1}{\frac{dy}{dx}(\beta')}\\
&\Leftrightarrow& \frac{\ln\beta\sin\theta + \cos\theta}{\ln\beta\cos\theta - \sin\theta} &= -\frac{\ln\beta'\cos\theta - \sin\theta}{\ln\beta'\sin\theta + \cos\theta}\\
&\Leftrightarrow& (\ln\beta\sin\theta + \cos\theta).(\ln\beta'\sin\theta + \cos\theta) &= -(\ln\beta'\cos\theta - \sin\theta).(\ln\beta\cos\theta - \sin\theta)\\
&\Leftrightarrow& \ln\beta\ln\beta'(\sin^2\theta+\cos^2\theta)+\cos^2\theta+\sin^2\theta &= (\ln\beta+\ln\beta')\cos\theta\sin\theta - (\ln\beta+\ln\beta')\sin\theta\cos\theta\\
&\Leftrightarrow& \ln\beta\ln\beta'+1 &= 0\\
&\Leftrightarrow& \ln\beta' &= \frac{-1}{\ln\beta}\\
&\Leftrightarrow& \beta' &= e^{-\ln^{-1}(\beta)}
\end{align*}
\begin{figure}
\centering
\includegraphics[width=.6\textwidth]{pics/orth_spiral.png}
\caption{\label{fig:orth_spiral}Une courbe orthogonale à une spirale logarithmique est une autre spirale logarithmique\protect\footnotemark. Les spirales colorées sont de la forme $\rho = \beta^{\theta}$ et les blanches sont de la forme $\rho = (\beta')^{\theta} = e^{-\theta\ln^{-1}\beta}$.}
\end{figure}
\footnotetext{\url{https://www.shadertoy.com/view/Xd3cWf}}

Nous obtenons un résultat cohérent, ce qui valide l'hypothèse ci-dessus (cf Fig. \ref{fig:orth_spiral}). Nous notons $n'$ le nombre de bras de la spirale orthogonale ainsi obtenue.
Nous pouvons alors déduire une paramétrisation orthogonale $(u,v)$ :
\begin{align*}
&&\rho &= \beta^{\theta + u\frac{2\pi}{n}}\\
&\Leftrightarrow&\beta^{u\frac{2\pi}{n}} &= \frac{\rho}{\beta^{\theta}}\\
&\Leftrightarrow&u\frac{2\pi}{n} &= \log_{\beta}\left(\frac{\rho}{\beta^{\theta}}\right)\\
&\Leftrightarrow&u &= \log_{\beta}\left(\frac{\rho}{\beta^{\theta}}\right)\frac{n}{2\pi}\\
&&\mathrm{et}\ \ \ v &= \log_{\beta'}\left(\frac{\rho}{\beta'^{\theta}}\right)\frac{n'}{2\pi}
\end{align*}

Cependant, la dimension en $v$ de cette paramétrisation est dépendante de $n'$, or nous voulons une paramétrisation conforme (en plus de la conservation de l'orthogonalité), afin de contrôler les paramètres du bruit sans introduire de distorsion. Pour cela, nous voulons donc exprimer $n'$ en fonction de $n$.

Afin de rendre notre paramétrisation conforme, il est nécessaire de normaliser les coordonnées, ce qui revient à rendre le maillage induit\footnote{Un maillage induit d'une paramétrisation est construit selon les isovaleurs des coordonnées, par exemple toutes les unités (cf Fig. \ref{fig:orth_spiral} et \ref{fig:reg_param}).}\ par la paramétrisation régulier (c'est-à-dire obtenir des côtés égaux pour les cellules du maillage).
\begin{figure}
\centering
\includegraphics[width=.3\textwidth]{pics/frenet.jpg}
\caption{\label{fig:frenet}Le repère tangent-radial, ou repère de Frenet. Les deux coordonnées évoluent le long de la tangente à une courbe (dans notre cas la spirale) et le long de la normale à cette courbe.}
\end{figure}
Pour cela, nous nous plaçons dans le repère tangent-radial (ou repère de Frenet \cite{repereFrenet}, cf Fig. \ref{fig:frenet}), qui permet de simplifier les formules qui suivent. La dérivée de la spirale à une distance $\rho$ devient dans ce repère :
\begin{align*}
(\rho d\theta&,\frac{d\rho}{d\theta}.d\theta)\\
=(\rho d\theta&,\rho\ln\beta.d\theta)
\end{align*}
qui est de longueur :
\begin{align*}
&\|(\rho d\theta,\rho\ln\beta.d\theta)\|\\
=\ &\rho\|(1,\ln\beta)\|d\theta
\end{align*}
Soit $\Delta\theta$ (resp. $\Delta\theta'$) la taille d'un côté d'une cellule le long de la spirale de paramètre $\beta$ (resp. le long de la spirale orthogonale de paramètre $\beta'$).
Nous voulons alors :
\begin{align*}
&&\rho\|(1,\ln\beta)\|\Delta\theta &= \rho\|(1,\ln\beta')\|\Delta\theta'\\
&\Leftrightarrow&\Delta\theta &= \frac{\|(1,\ln\beta')\|}{\|(1,\ln\beta)\|}\Delta\theta'\\
&\Leftrightarrow&\Delta\theta &= \|\ln\beta'\|\frac{\|(\frac{1}{\ln\beta'},1)\|}{\|(1,\ln\beta)\|}\Delta\theta'\\
&\Leftrightarrow&\Delta\theta &= \left\|\frac{-1}{\ln\beta}\right\|\frac{\|(-\ln\beta,1)\|}{\|(1,\ln\beta)\|}\Delta\theta'\\
&\Leftrightarrow&\Delta\theta &= \frac{1}{\ln\beta}\Delta\theta'
\end{align*}

Par définition de  $\Delta\theta$, nous obtenons :
\[\Delta\theta.n = \Delta\theta'.n' = 2\pi\]
\[\Rightarrow\hspace{1cm} n' = \frac{1}{\ln\beta}n\]

Nous obtenons alors une paramétrisation conforme (cf Fig. \ref{fig:reg_param}).

\clearpage
\begin{figure}[!h]
\centering
\includegraphics[width=.8\textwidth]{pics/reg_param.png}
\caption{\label{fig:reg_param}Le maillage régulier obtenu grâce à la paramétrisation conforme\protect\footnotemark\ (ici pour $n=12$). Il est à noter que $n'$ étant arrondi à l'entier le plus proche, la paramétrisation n'est pas exactement conforme (néanmoins suffisante pour manipuler le bruit). La ligne rouge (resp. verte) représente la discontinuité avant notre correction (resp. après notre correction).}
\end{figure}

\footnotetext{\url{https://www.shadertoy.com/view/lsKyz1}}

\section{Correction de la discontinuité de la paramétrisation}

La paramétrisation obtenue dans la section \ref{sec:parametrisation} posera un problème de discontinuité lors de l'implémentation : la fonction arctan (nécessaire à la transposition en coordonnées polaires) crée une discontinuité de $-\pi$ à $\pi$ (ligne rouge sur la Fig. \ref{fig:reg_param})\footnote{Ou de $0$ à $2\pi$, selon l'implémentation.}. Cette discontinuité peut être corrigée en normalisant $u$ par rapport à $n$ :
\begin{align*}
&u' = u \Mod n &v' = v + n' \left\lfloor\frac{u}{n}\right\rfloor
\end{align*}

Nous obtenons ainsi une paramétrisation $(u',v')$ continue le long de chaque bras de la spirale (cf Fig. \ref{fig:reg_param}). Nous notons par la suite cette paramétrisation $(u,v)$.
Cependant, cette normalisation induit une nouvelle discontinuité entre deux bras (ligne verte sur la Fig. \ref{fig:reg_param}). Cette nouvelle discontinuité sera à prendre en compte lors du plaquage du bruit le long de la spirale, en introduisant des conditions explicites au bord de la discontinuité, ou bien en utilisant un bruit cyclique.\\
\textbf{Note :} Il n'est pas possible d'obtenir une paramétrisation conforme continue d'une spirale logarithmique. Nous choisissons de placer la discontinuité entre deux bras le long de la coordonnée $v$, car une discontinuité le long d'une seule des deux coordonnées rend plus simple les solutions permettant de la prendre en compte.

\section{\label{sec:observation}Observation de la structure des nuages de poussière dans l'espace de la spirale logarithmique}

Afin d'observer la structure des nuages de poussière dans l'espace paramétrique de la spirale logarithmique (dans lequel nous souhaitons les modéliser), nous inversons la paramétrisation pour transposer les coordonnées $(u,v)$ en polaire $(\rho,\theta)$ (et ainsi en cartésien, cf Fig. \ref{fig:pol}):
\begin{align*}
&&\rho &= \alpha\beta^{\theta + 2\pi\frac{u}{n}} &\rho &= \alpha\beta'^{\theta + 2\pi\frac{v}{n'}}\\
&\Leftrightarrow&\rho &= \alpha\beta^{\theta + 2\pi\frac{u}{n}} &\beta^{\theta + 2\pi\frac{u}{n}} &= \beta'^{\theta + 2\pi\frac{v}{n'}}\\
&\Leftrightarrow&&"&\theta + 2\pi\frac{u}{n} &= \frac{\ln\left(\beta'^{\theta + 2\pi\frac{v}{n'}}\right)}{\ln\beta}\\
&\Leftrightarrow&&"&\theta + 2\pi\frac{u}{n} &= \left(\theta + 2\pi\frac{v}{n'}\right)\frac{\ln\beta'}{\ln\beta}\\
&\Leftrightarrow&&"&\left(\theta + 2\pi\frac{u}{n}\right)\ln\beta &= \left(\theta + 2\pi\frac{v}{n'}\right)\ln\beta'\\
&\Leftrightarrow&&"&\theta\ln\beta-\theta\ln\beta' &= 2\pi\frac{v}{n'}\ln\beta' - 2\pi\frac{u}{n}\ln\beta\\
&\Leftrightarrow&&"&\theta(\ln\beta-\ln\beta') &= 2\pi\frac{v}{n'}\ln\beta' - 2\pi\frac{u}{n'}\\
&\Leftrightarrow&\rho &= \alpha\beta^{\theta + 2\pi\frac{u}{n}}& \theta &= \frac{\frac{2\pi}{n'}(v\ln\beta' - u)}{\ln\beta-\ln\beta'}
\end{align*}

Grâce à cette inversion de la paramétrisation, nous obtenons cette représentation de M51 (Fig. \ref{fig:poster}), qui nous permet d'observer la galaxie dans l'espace paramétrique de la spirale logarithmique, et de mesurer les propriétés suivantes sur les nuages de poussière :
\begin{itemize}
\item les filaments des bras forment des lignes droites horizontales, légèrement perturbées;
\item les \textit{spurs} forment des angles de 60° avec les bras, et apparaissent de longueur, d'espacement et d'épaisseur similaires le long des bras (malgré quelques irrégularités dans ces propriétés);
\item les nuages de poussière apparaissent bruités et anisotropes selon une orientation proche de celle des \textit{spurs}.
\end{itemize}

La régularité de ces observations selon cet espace paramétrique confirme notre choix du modèle de spirale logarithmique. Nous nous servons donc de cette paramétrisation pour modéliser les nuages de poussière.

\clearpage
\begin{figure}
\centering
\includegraphics[width=\textwidth]{pics/Poster_M51.png}
~\\
\includegraphics[width=\textwidth]{pics/Poster_M51_highlighted.png}
\caption{\label{fig:poster}Représentation de M51 dans l'espace paramétrique de la spirale logarithmique. Nous avons retouché les couleurs pour faire ressortir en violet les nuages de poussière. Dans le coin inférieur droit, la masse claire est le bulbe de la galaxie. Nous observons les bras alignés avec l'axe horizontal, qui se répètent verticalement selon la paramétrisation (le bras en bas de l'image est le même que le bras visible en haut de l'image)\protect\footnotemark. Sur l'image du bas, les bras sont marqués en jaune, et les \textit{spurs} en turquoise. Nous observons que les bras forment des lignes droites horizontales, et que les \textit{spurs} sont relativement réguliers le long des bras (orientation, taille, espacement).}
\end{figure}

\footnotetext{\url{https://www.shadertoy.com/view/lsKcWc}}

\clearpage
\section{\label{sec:modelisation}Modélisation des nuages de poussière}

%Nous proposons une modélisation sous forme de filaments des nuages de poussière, à l'échelle des filaments internes aux bras et des \textit{spurs}. Nous représentons des veines de la forme des bras et des \textit{spurs}, que nous interprétons comme facteurs de densité (un facteur peu important pour la zone inter-bras où les nuages sont peu denses et peu structurés, et un facteur important pour les bras et les \textit{spurs} où les nuages sont concentrés). Nous utilisons ensuite ces facteurs de densité pour modéliser la densité des nuages de poussière fractals, puis nous en déduisons une opacité.
Nous proposons une modélisation sous forme de filaments des nuages de poussière, à l'échelle des filaments internes aux bras et des \textit{spurs}. Nous nous inspirons des techniques présentées la littérature permettant de former des veines bruitées \cite{ebert2003texturing}\cite{perlin1989hypertexture}. Parmi ces techniques, le \textit{charcoal} consiste à former un prototype basse fréquence de la forme globale à modéliser, et à le combiner avec un bruit procédural ajoutant les détails haute fréquence. Nous utilisons également le principe de veine de marbre, basé sur un bruit basse fréquence dont nous nous servons pour perturber la veine (cette technique est appelée \textit{displacement}).

Nous cherchons dans un premier temps à former le prototype basse résolution de la structure des nuages de poussière (des veines formant les filaments des bras et les \textit{spurs}). Nous choisissons de définir leur profil grâce à une fonction gaussienne\footnote{Il reste possible de choisir toute autre fonction formant un pic, selon le modèle de veines retenu.}, qui présente l'avantage d'être modulable en largeur. Nous interprétons ce prototype comme facteur de densité basse résolution (un facteur peu important pour la zone inter-bras où les nuages sont peu denses et peu structurés, et un facteur important pour les bras et les \textit{spurs} où les nuages sont concentrés). Nous utilisons ensuite ces facteurs pour modéliser la densité haute résolution des nuages de poussière fractals grâce à un bruit de Perlin multiplicatif, puis nous en déduisons une opacité.

\subsection{Modélisation des filaments des bras}

Dans un premier temps, nous modélisons les veines basses résolution formant les filaments des bras. Nous utilisons pour cela une fonction gaussienne périodique (la périodicité de la fonction sert à former les différents bras). Nous appliquons alors simplement cette fonction selon la coordonnée $u$ de notre paramétrisation avec une période $1$ pour obtenir une veine pour chaque bras : $$Veine_{bras}(u,v) = PeriodGauss(u)$$

Afin d'obtenir des veines de forme légèrement irrégulières comme c'est le cas pour les nuages de poussière dans les bras des galaxies spirales, nous perturbons notre veine\footnote{Nous modifions ici uniquement la forme de la veine. Le bruit représentant les densités de nuages sera appliqué dans la Section \ref{sec:bruitmult}.} par \textit{displacement} : $$Veine_{bras}(u,v) = PeriodGauss(u+Perturb(u,v))$$
avec $Perturb:\mathbb{R}^2\rightarrow[-1,1]$ le bruit de Perlin additif (nous utilisons ce bruit car son aspect lisse est adapté au léger \textit{displacement} que nous cherchons à appliquer).

Une utilisation naïve d'un bruit de Perlin additif classique rendrait visible la discontinuité de la paramétrisation (Fig. \ref{fig:veine_bras}). Il est donc nécessaire d'utiliser un bruit de Perlin cyclique (de cycle $(n,n')$, qui est le plus grand cycle permettant d'assurer la continuité dans notre paramétrisation) pour perturber la veine de manière continue dans l'espace de la spirale (cf Fig. \ref{fig:veine_bras}).

\begin{figure*}
\centering
 \begin{subfigure}[t]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{pics/veine_bras_bruite_discontinu.jpg}
 \end{subfigure}%
 ~
 \hspace{0.05\textwidth}
 \begin{subfigure}[t]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{pics/veine_bras_bruite_continu.jpg}
 \end{subfigure}
 \caption{\label{fig:veine_bras}À gauche, l'approche naïve du \textit{displacement} fait apparaître la discontinuité de la paramétrisation. À droite, l'utilisation d'un bruit de Perlin cyclique permet de conserver une veine continue. La ligne rouge représente la discontinuité de la paramétrisation.}
\end{figure*}

\subsection{Modélisation des filaments des \textit{spurs}}

Dans un deuxième temps, nous modélisons les veines des \textit{spurs}. Comme nous l'avons vu dans les Section \ref{sec:struct_nuages} et \ref{sec:observation}, les \textit{spurs} partent à environ 60° des filaments des bras, puis reprennent la même orientation que ces derniers dans la zone inter-bras. Leur forme prototypale n'ayant pas encore été déterminée, nous proposons une modélisation de leur forme comme une fonction quelconque (dans notre implémentation, nous avons choisi de les représenter comme branche de parabole (Fig. \ref{fig:model_spurs})).

Pour cela, nous utilisons également une fonction gaussienne périodique à partir de notre paramétrisation $(u,v)$ : $$Veine_{\mathit{spurs}}(u,v) = PeriodGauss(v+f(u))$$ avec $f$ la fonction décrivant la forme des \textit{spurs} (cf Fig. \ref{fig:model_spurs}).

\begin{figure*}
\centering
 \begin{subfigure}[t]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{pics/f_spurs.jpg}
 \caption{Fonction de modélisation des \textit{spurs}. En orange, l'axe $y$ représente la veine du bras. Ici, $f(x)=2(x+\delta)^2$, avec $\delta$ le décalage permettant d'obtenir l'angle d'incidence de 60°$=\frac{\pi}{3}$ des \textit{spurs}. Pour obtenir $\delta$, nous voulons \mbox{$f'(\delta)=\tan\left(\frac{\pi}{2}-\frac{\pi}{3}\right)\ \Rightarrow\ \delta = \frac{1}{4}\tan\frac{\pi}{6}$}}
 \end{subfigure}%
 ~
 \hspace{0.05\textwidth}
 \begin{subfigure}[t]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{pics/veine_bras_spurs.jpg}
 \caption{Les veines des bras et des \textit{spurs} (ici non perturbées), selon la fonction définie ci-contre (la parabole \textit{spurs} est tronquée pour n'apparaître que vers l'extérieur des bras, selon la partie de la fonction à droite de la ligne orange dans le graphique ci-contre). Ils sont également estompés en fonction de la distance au bras.}
 \end{subfigure}
 \caption{\label{fig:model_spurs}Modélisation des veines des \textit{spurs} à partir d'une fonction.}
\end{figure*}

Les veines des \textit{spurs} sont perturbées par \textit{displacement} avec le même bruit que les veines des bras, pour conserver la cohérence des points d'incidence entre \textit{spurs} et bras : $$Veine_{\mathit{spurs}}(u,v) = PeriodGauss((v+Perturb(u,v))+f(u+Perturb(u,v)))$$
Enfin, les veines des bras et des \textit{spurs} sont combinées pour produire la fonction de veines finale : $$Veines = max(Veine_{bras},Veine_{\mathit{spurs}})$$

\subsection{\label{sec:bruitmult}Ajout du bruit procédural}

Nous utilisons un bruit de Perlin multiplicatif cyclique pour représenter les hautes fréquences de densités des nuages de poussière, en raison de ses propriétés mimétiques de la formation des nuages (cf Section \ref{sec:procedural}). Nous appliquons le bruit selon la paramétrisation $(u,v)$, avec un cycle $(n,n')$ (Fig. \ref{fig:mapped_noise}).

\begin{figure*}[h!]
\centering
 \begin{subfigure}[t]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{pics/mapped_noise.png}
 \end{subfigure}%
 ~
 \hspace{0.05\textwidth}
 \begin{subfigure}[t]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{pics/mapped_noise_grid.png}
 \end{subfigure}
 \caption{\label{fig:mapped_noise}Bruit multiplicatif cyclique isotrope continu appliqué selon la paramétrisation de spirale logarithmique. Il est possible de repérer les similarités dues au cycle, néanmoins leur différence d'échelle induite par la paramétrisation logarithmique rend ces similarités peu détectables en pratique. Sur l'image de droite, le maillage est affiché en turquoise (il est possible de remarquer le cycle de $(n,n')=(2,6)$ sur cette image).}
\end{figure*}

Le fait d'utiliser la paramétrisation de la spirale pour appliquer le bruit permet de le contrôler relativement à cette paramétrisation : si nous voulons appliquer au bruit une anisotropie qui suive l'orientation constatée dans la Section \ref{sec:observation}, nous pouvons l'ajouter par simple déformation (rotation et étirement) homogène des coordonnées de la paramétrisation (Fig. \ref{fig:mapped_anis_noise}).

\begin{figure*}
\centering
 \begin{subfigure}[t]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{pics/mapped_anis_noise.png}
 \end{subfigure}%
 ~
 \hspace{0.05\textwidth}
 \begin{subfigure}[t]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{pics/mapped_anis_noise_disc.png}
 \end{subfigure}
 \caption{\label{fig:mapped_anis_noise}Ajout d'une anisotropie au bruit multiplicatif (selon le modèle de comportement des nuages de poussière de \cite{spurs_formation} et nos observation dans la Section \ref{sec:observation}). L'anisotropie ajoutée est homogène dans la paramétrisation de spirale logarithmique afin d'obtenir une orientation cohérente. Nous observons que la discontinuité de la paramétrisation (en rouge sur l'image de droite) a un impact sur la continuité du bruit (il n'est plus continu le long de la discontinuité de la paramétrisation). Cela est dû au calcul permettant la cyclicité du bruit qui n'est plus cohérent lorsque les coordonnées ne sont plus orthogonales à la discontinuité, ce qui se produit lors de l'ajout de l'anisotropie.}
\end{figure*}

Le bruit homogène ainsi obtenu est ensuite modulé par les veines obtenues précédemment. Les veines sont une fonction $Veines : \mathbb{R}^2 \rightarrow [0,1]$. Nous l'interprétons comme facteur de densité (élevé dans les bras et les \textit{spurs}, peu élevé dans la zone inter-bras). Nous obtenons ainsi une densité finale : $$Densite = BruitMult\times(Veines\times(D_h-D_l)+D_l)$$
où $D_h$ et $D_l$ sont les facteurs de densité respectivement dans les veines et dans la zone inter-bras.

Afin d'obtenir à partir de notre approche 2D des résultats approchant les images réelles de nuages de poussière, nous interprétons la densité obtenue ci-dessus vers une opacité : $$Opacite = 1-e^{-Densite \times W \times \sigma}$$
où $W$ représente l'épaisseur de la couche de nuages et $\sigma$ représente le facteur d'absorption des nuages. Cette transformation est cohérente avec les modèles physiques 3D de propagation de lumière dans des milieux partiellement opaques (tels que les nuages de poussière)\cite{Guillaumereport}. Nous obtenons ainsi des valeurs d'opacité (Fig. \ref{fig:final1}), qui pourront être utilisées pour masquer la lumière des étoiles.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{pics/final_1.png}
\caption{\label{fig:final1}Opacité finale obtenue par combinaison des veines et du bruit de Perlin multiplicatif\protect\footnotemark. Cette image est obtenue pour $D_h=5.6$ et $D_l=0.6$, devant un fond blanc uniforme. L'intégration dans le modèle de galaxie complet est présenté en Section \ref{sec:results}.}
\end{figure}

\footnotetext{\url{https://www.shadertoy.com/view/lsyfRh}}


\clearpage
\subsection{Degrés de libertés disponibles}

Notre approche offre, par sa construction, de nombreux paramètres permettant aux graphistes (ou aux ingénieurs de RSA Cosmos) d'ajuster le modèle. Ces paramètres ont pour but d'offrir une manipulation directe du modèle afin de correspondre au mieux à la Voie Lactée, ou à toute autre galaxie spirale.

\noindent
Premièrement, nous avons des paramètres offrant un contrôle sur la géométrie des veines (donc sur la structure des nuages de poussière à grande et moyenne échelle) :
\begin{itemize}
\item la forme générale des bras spiraux est directement contrôlée par $\beta$ et $n$;
\item la largeur des bras (resp. des \textit{spurs}) est directement contrôlée par la largeur de la fonction gaussienne périodique utilisée pour les bras (resp. pour les \textit{spurs});
\item le nombre de \textit{spurs} est directement contrôlé par la période de la fonction gaussienne périodique utilisée pour ces derniers;
\item la forme des \textit{spurs} et leur angle d'incidence sont directement contrôlés par la fonction choisie pour les modéliser (cf Fig.\ref{fig:model_spurs});
\item la longueur des \textit{spurs} est directement contrôlée par la distance d'estompage de ces derniers;
\item la fréquence et l'amplitude de la perturbation appliquée aux veines sont contrôlées par simples produits : $Ampl\times Perturb(Freq\times(u,v))$.
\end{itemize}
\noindent
Deuxièmement, nous avons des paramètres offrant un contrôle sur l'application du bruit de Perlin multiplicatif (donc sur la structure des nuages de poussière à petite échelle) :
\begin{itemize}
\item l'anisotropie du bruit est contrôlée dans l'espace de la spirale;
\item les densités des nuages de poussière dans et en dehors des veines sont contrôlées directement et indépendamment par $D_h$ et $D_l$.
\end{itemize}
\noindent
Enfin, nous avons un paramètre offrant un contrôle sur l'aspect des nuages de poussière à partir de la densité calculée :
\begin{itemize}
\item l'épaisseur de la couche de nuages considérée est directement contrôlée par $W$;
\item le facteur d'absorption des nuages de poussière est directement contrôlé par $\sigma$.
\end{itemize}