\chapter{\label{chap:prob}Phénoménologie des galaxies spirales, état de l'art et problématique}

%Les galaxies sont parmi les plus grands objets célestes visibles. Composées de centaines de milliards d'étoiles, ainsi que de nébuleuses et de nuages de poussière, nous ne pouvons les observer que sous l'angle duquel nous les voyons depuis la Terre.

%Pourtant, les galaxies sont des objets majestueux méritant pour la plupart d'être vues plus en détails. C'est pourquoi nous nous intéressons à les simuler sur ordinateur, permettant de profiter au mieux de ces structures uniques.

Les galaxies sont parmi les plus grands objets célestes visibles. Elles sont composées de centaines de milliards d'étoiles, ainsi que de nébuleuses et de nuages de poussière. Hélas, nous ne pouvons les observer que sous l'angle dont nous les voyons depuis la Terre. Cependant, à des fins de vulgarisation scientifique, il est intéressant de pouvoir les observer sous d'autres angles, de l'intérieur comme de l'extérieur. C'est pourquoi nous nous intéressons à les simuler sur ordinateur, afin d'en obtenir des visualisations tridimensionnelles utilisables en planétariums.

\section{Phénoménologie : les galaxies spirales}
Parmi les galaxies, les plus spectaculaires sont les galaxies spirales. Elles prennent la forme d'un disque, dont le cœur, en général sphérique et lumineux, est appelé le bulbe. Dans le plan du disque, nous trouvons plusieurs bras enroulés en spirale, où se concentrent les étoiles les plus brillantes et les nuages de gaz et de poussière (cf Fig. \ref{fig:m51base}).
Une des théories les plus probables concernant les bras des galaxies spirales est qu'ils ne sont pas composés de matière propre aux bras. Ils seraient en réalité des ondes de densité, concentrant ainsi la matière présente \cite{courtes1971evidence}, provoquant des flambées d'apparition de nouvelles étoiles.

\begin{figure*}
\centering
  \begin{subfigure}[t]{.47\textwidth}
  \centering
  \includegraphics[width=\linewidth]{pics/M51_cut2.jpg}
  \caption{\label{fig:m51base}Photo de M51 prise par le satellite \textit{Hubble} (fausses couleurs).
  Nous observons notamment en jaune clair au centre un bulbe de gaz, en bleu clair les concentrations d'étoiles dans les bras spiraux, en rouge les nébuleuses et en brun sombre les nuages de poussière. Les nuages de poussière apparaissent sombres car ils masquent la luminosité des étoiles qui sont situées dans et derrière eux.}
  \end{subfigure}%
  ~
  \hspace{.05\textwidth}
  \begin{subfigure}[t]{0.47\textwidth}
  \centering
  \includegraphics[width=\linewidth]{pics/M51_highlighted.jpg}
  \caption{\label{fig:m51highlighted}Structure globale des nuages de poussière. En jaune, des filaments importants suivent les bras. En turquoise, des filaments secondaires (les \textit{spurs}) partent des bras vers l'extérieur. En vert, les trainées des \textit{spurs} s'alignent avec les bras \cite{carlqvist2013model}.}
  \end{subfigure}
  \caption{\label{fig:m51}Une galaxie spirale : M51, ou Galaxie du Tourbillon \cite{wiki_galaxies}}
\end{figure*}


\subsection{La Voie Lactée}
La galaxie dans laquelle est située la Terre est la Voie Lactée. Contrairement aux autres galaxies visibles dans le ciel, la Voie Lactée est difficilement observable : nous sommes situés à l'intérieur (cf Fig. \ref{fig:milkywayinside}). Cependant, des observations effectuées par les astronomes ont permis d'en comprendre la structure : nous savons que notre galaxie est une galaxie spirale. Elle possède deux bras principaux, ainsi que  deux bras moins importants \cite{milkyway_carto}. De plus, nous savons depuis peu que le cœur de la Voie Lactée forme une barre \cite{benjamin2005first} (cf Fig. \ref{fig:milkyway_artist}).

\begin{figure}
\centering
\includegraphics[width=.7\textwidth]{pics/MilkyWay.jpg}
\caption{\label{fig:milkywayinside}Vue cylindrique de la Voie Lactée depuis la Terre. Le fait d'être à l'intérieur de la galaxie la rend difficile à observer : la sonde spatiale la plus éloignée de la Terre, \textit{Voyager 1}, est toujours dans le système Solaire \cite{voyager}. Nous n'aurons donc sans doute jamais d'images réelles depuis l'extérieur de la galaxie. Néanmoins, nous sommes en mesure de capter certaines informations par mesure de profondeur des objets observables (notamment grâce au satellite \textit{Gaia} \cite{gaia}), mais ces informations restent très incomplètes.}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=.5\textwidth]{pics/milkyway_artistview_4.jpg}
\caption{\label{fig:milkyway_artist}Reconstitution de la structure de la Voie Lactée (NASA, \cite{milkyway_carto}). Nous observons le cœur en forme de barre au centre, d'où partent les deux bras spiraux principaux. La position du Soleil est représentée sur cette carte au centre de mire, au centre inférieur de l'image.}
\end{figure}

\subsection{Images de galaxies spirales}
Afin de mieux comprendre la structure de la Voie Lactée, une solution est de se référer à des images d'autres galaxies spirales (que nous supposons similaires à la Voie Lactée). L'avantage est de pouvoir disposer d'images prises depuis l'extérieur de ces galaxies, afin d'en avoir une vue globale.

Cependant, de telles images exploitables dans ce but sont rares. En effet, il est nécessaire de trouver une galaxie spirale suffisamment proche de nous pour en avoir une image de résolution satisfaisante, mais également dont le disque soit orienté face à nous, afin de réduire les effets de distorsion dus à la perspective. À ce jour, de telles galaxies se comptent sur les doigts d'une main (cf Section \ref{sec:ref_images}).

Ces images sont fournies par la NASA et prises pour la plupart par le satellite \textit{Hubble}. Les couleurs de ces images sont fausses : elles sont le résultat d'une fusion d'images prises dans différents canaux électromagnétiques (larges ou étroits) \cite{process_hubble}. L'observation de ces différentes longueurs d'ondes peut permettre de mieux observer certains éléments, comme les nuages de poussière (cf Fig. \ref{fig:m51ir}).

\begin{figure}[!h]
\centering
\includegraphics[width=.53\textwidth]{pics/M51_Visible_IR_2.jpg}
\caption{\label{fig:m51ir}\textit{Hubble} capte des images sur plusieurs canaux, comme ici : à gauche dans le visible, où les nuages de gaz sont absorbeurs (ils apparaissent donc sombres), et à droite dans l'infra-rouge lointain, dans lequel les nuages sont émetteurs.}
\end{figure}

\section{\label{sec:CG}État de l'art : l'informatique graphique}

L'informatique graphique est souvent utilisée pour représenter des scènes réelles ou des mondes virtuels de manière réaliste. À des fins de vulgarisation, les planétariums cherchent à produire des visualisations en temps réel (c'est-à-dire autour de 30 images par secondes au minimum) des objets célestes, dont les galaxies (et notamment la Voie Lactée), grâce à l'informatique graphique.

Pour représenter une scène, la méthode usuelle en informatique graphique est d'obtenir un modèle explicite et exact de chaque élément de la scène. Cependant, dans le cadre du rendu de galaxies, il n'est pas envisageable de représenter des centaines de milliards d'étoiles  de manière explicite (de plus, nous ne disposons pas de telles informations : nous ne connaissons la position exacte que d'une infime partie des étoiles de notre galaxie). Il est donc nécessaire de trouver d'autres solutions, comme des modèles plus généraux ou des fonctions procédurales\footnote{En informatique graphique, une fonction procédurale est un moyen de définir un modèle de manière implicite, ce qui permet d'économiser de l'espace de stockage au prix de quelques approximations (cf Section \ref{sec:procedural}).}.

\subsection*{Le projet veRTIGE}

De 2010 à 2014, des astrophysiciens de l'Observatoire de Paris-Meudon, l'entreprise de planétariums RSA Cosmos et l'équipe Maverick (Inria, Cnrs) se sont intéressés au rendu de galaxie (en l'occurrence la Voie Lactée) avec le projet veRTIGE (visually enhanced Real-Time Interactive Galaxy for Education) \cite{pres_vertige}.

Ce projet avait pour but de proposer une visualisation tridimensionnelle de la Voie Lactée. Cette visualisation devait répondre aux contraintes :
\begin{itemize}
\item d'être temps réel;
\item de fonctionner sur les écrans ultra-haute résolution des planétariums;
\item de proposer des images de même qualité que les photos produites par le satellite \textit{Hubble};
\item de permettre des points de vue à différentes échelles, et de l'intérieur comme de l'extérieur de la Voie Lactée.
\end{itemize}
\noindent
veRTIGE s'est basé sur les données suivantes :
\begin{itemize}
\item des images de référence (notamment les photos de \textit{Hubble}, mais aussi des photos prises depuis des observatoires sur Terre);
\item la simulation astrophysique GalMer;
\item des études de cas et de phénoménologie astrophysiques.
\end{itemize}

GalMer est une base de données de simulations physiques de galaxies \cite{chilingarian2010galmer}. Son but est de valider des modèles d'évolution de galaxies, et est utilisée habituellement par les astrophysiciens. Dans le cadre du projet veRTIGE, la simulation GalMer a servi de modèle basse résolution pour la structure de la Voie Lactée (cf Fig. \ref{fig:galmer}).
\begin{figure*}
\centering
  \begin{subfigure}[t]{0.46\textwidth}
  \centering
  \includegraphics[width=\linewidth]{pics/veRTIGE_1_(galmer)_contrast.png}
  \caption{\label{fig:galmer1}Le champ d'étoiles (il apparait diffus en raison du nombre et de la taille des étoiles à cette échelle)}
  \end{subfigure}%
  ~
  \hspace{.05\textwidth}
  \begin{subfigure}[t]{0.46\textwidth}
  \centering
  \includegraphics[width=\linewidth]{pics/veRTIGE_2_(galmer)_contrast.png}
  \caption{\label{fig:galmer2}Le champ d'étoiles et les nuages de gaz et de poussière}
  \end{subfigure}
  \caption{\label{fig:galmer} Resultat de la simulation GalMer de la Voie Lactée (version de production pour veRTIGE) \cite{pres_vertige}}
\end{figure*}

Le projet veRTIGE a abouti à la visualisation d'une simulation respectant de manière réaliste les statistiques de disposition des étoiles dans la Voie Lactée, en utilisant des fonctions procédurales pour représenter les nuages de poussière (cf Fig. \ref{fig:vertige1}). Cependant, ces nuages ont été modélisés sans chercher à en respecter la structure réelle \cite{comm_perso_vertige}. Or, la structure des nuages est un élément important permettant d'apporter du relief à la visualisation. C'est pourquoi nous allons nous intéresser aux nuages de poussière galactiques dans les galaxies spirales, et à leur structure.
\begin{figure*}
\centering
  \begin{subfigure}[t]{0.46\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{pics/veRTIGE_3_contrast.png}
  \caption{\label{fig:vertige1}Ajout de bruit procédural 3D}
  \end{subfigure}%
  ~
  \hspace{.05\textwidth}
  \begin{subfigure}[t]{0.46\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{pics/veRTIGE_4_contrast.png}
  \caption{\label{fig:vertige2}Ajout d'amas d'étoiles et de nébuleuses}
  \end{subfigure}
  \caption{\label{fig:vertige} Résultat du projet veRTIGE \cite{pres_vertige}}
\end{figure*}

\section{\label{sec:struct_nuages}Phénoménologie : la structure des nuages de poussière galactiques}

Les nuages de poussière dans les galaxies spirales adoptent des structures différentes selon l'échelle.

À grande échelle, les nuages de poussière s'organisent en filaments qui suivent la forme des bras spiraux (cf Fig. \ref{fig:m51highlighted}).

À moyenne échelle, nous observons que des filaments plus petits partent des bras (principalement vers l'extérieur \cite{carlqvist2013model}\cite{elmegreen1980properties}). Ces filaments sont appelés \textit{spurs} et ont une structure moins définie au milieu de la zone inter-bras. Ils semblent former de nouveaux filaments dans l'axe des bras \cite{dobbs2006spurs} (cf Fig. \ref{fig:m51highlighted}, \ref{fig:m51spur}).

\begin{figure*}
\centering
  \begin{subfigure}[t]{.47\textwidth}
  \centering
  \includegraphics[width=\linewidth]{pics/M51_zoom.jpg}
  \end{subfigure}%
  ~
  \hspace{.05\textwidth}
  \begin{subfigure}[t]{0.47\textwidth}
  \centering
  \includegraphics[width=\linewidth]{pics/M51_zoom_highlighted.jpg}
  \end{subfigure}
  \caption{\label{fig:m51spur}Zoom sur la zone inter-bras de M51. Les deux filaments sombres courbés (en jaune sur l'image de droite) sont les deux bras de la galaxie (le bulbe étant situé vers le haut de l'image). Nous observons des \textit{spurs} partant de chaque bras vers l'extérieur. Leur orientation à proximité du bras dont ils partent (en turquoise) diffère d'environ 60° par rapport au bras. À mesure qu'ils s'éloignent du bras, ils forment de nouveaux filaments alignés avec les bras (en vert) \cite{dobbs2006spurs}.}
\end{figure*}

En-dessous d'une certaine échelle, la structure des nuages de poussière devient fractale, et leur apparence est chaotique \cite{fcombes} (cf Fig. \ref{fig:milkywaycloud}). Cette structure est due à la dispersion et à l'effondrement par auto-gravité du gaz dans un milieu initialement homogène, ce qui provoque des concentrations de matière à toutes les échelles, en structure fractale \cite{pfenniger1993dark2}. Les nuages à basses échelles peuvent présenter une anisotropie (ils apparaissent orientés dans une certaine direction), selon la même orientation que les \textit{spurs} \cite{spurs_formation}.
\begin{figure}
\centering
\includegraphics[width=.5\textwidth]{pics/MilkyWayCloud.jpg}
\caption{\label{fig:milkywaycloud}À petite échelle, la structure en filaments des nuages est moins présente, et ils se divisent en amas de différentes tailles, ce qui se résume à un aspect fractal \cite{fcombes}.}
\end{figure}

\section{\label{sec:procedural}État de l'art : le bruit procédural}

Il n'est pas envisageable de stocker explicitement la structure fractale et chaotique des nuages. Il est donc nécessaire de trouver une solution pour modéliser implicitement cette structure. Une méthode courante en informatique graphique pour résoudre ce problème est l'utilisation de bruits procéduraux.

Les bruits procéduraux sont des fonctions pseudo-aléatoires, et sont souvent utilisés en informatique graphique pour perturber des éléments d'une scène, notamment pour y ajouter des irrégularités (par exemple, un effet de crépi sur un mur). Ils peuvent également être utilisés pour former intégralement des volumes hétérogènes \cite{perlin1989hypertexture}.

Les bruits usuels sont :
\begin{itemize}
\item continus,
\item $f : \mathbb{R}^d\rightarrow[-1,1]$, avec une moyenne à $0$,
\item isotropes (pas de direction privilégiée),
\item non-périodiques,
\item de fréquence contrôlable.
\end{itemize}

La littérature compte plusieurs bruits répondant aux propriétés ci-dessus \cite{lagae2010survey}, comme le bruit de Perlin \cite{perlin1985image}, le Wavelet noise \cite{cook2005wavelet}, ou le Gabor noise \cite{galerne2012gabor}.

Ces bruits sont souvent combinés à partir de plusieurs fréquences pour obtenir un aspect détaillé. Nous nous intéressons ici pour la modélisation des nuages de poussière au bruit de Perlin et aux techniques de combinaisons de fréquences.

\subsection*{Le bruit de Perlin additif et multiplicatif}

Le bruit de Perlin classique (additif) est obtenu par additions successives des fréquences calculées dans $[-1,1]$, ce qui produit un bruit d'aspect relativement lisse (cf Fig. \ref{fig:frequences_perlin}). Ce bruit est fréquemment utilisé pour effectuer des perturbations sur une surface \cite{perlin1989hypertexture}.


\begin{figure}[!h]
\centering
\includegraphics[width=.9\textwidth]{pics/octaves_perlin_2.png}
\caption{\label{fig:frequences_perlin}Combinaisons de fréquences pour les bruits de Perlin 2D. En haut : bruit de Perlin additif (classique). En bas : bruit de Perlin multiplicatif (Source : \cite{Guillaumereport} et veRTIGE). De gauche à droite, ajout successifs de fréquences.}
\end{figure}

Afin de reproduire au mieux l'aspect des nuages de poussière, une possibilité est de s'inspirer des phénomènes de formation de ces nuages. Nous cherchons donc à construire un bruit d'une manière approchant les phénomènes de dispersion et d'effondrement à l'œuvre dans les nuages (cf Section \ref{sec:struct_nuages}). Ceci peut être obtenu en utilisant une variante du bruit de Perlin additif : le bruit de Perlin multiplicatif (non publié, mais exploré par \cite{Guillaumereport} et veRTIGE). Dans ce cas, les fréquences sont multipliées au lieu d'être additionnées, ce qui produit un bruit ayant un aspect plus rugueux (cf Fig. \ref{fig:frequences_perlin}), et plus proche des nuages galactiques visuellement (cf Fig. \ref{fig:milkywaycloud}) et par sa formation : les fréquences sont comprises entre $0$ et $2$, avec une moyenne à $1$; les multiplications successives conservent la moyenne, tout en concentrant les pics à différentes échelles (selon l'idée d'\textit{Iterated Function System} (ou IFS) fractal \cite{barnsley1985iterated}).

Ces deux bruits présentent une propriété de non-périodicité. Cependant, nous pouvons avoir besoin de les rendre cycliques. Les fréquences du bruit de Perlin étant basées sur une fonction pseudo-aléatoire calculée à partir d'un noyau de valeurs\protect\footnotemark, il est possible d'utiliser un modulo pour obtenir un noyau cyclique, et donc un bruit cyclique continu, de cycle contrôlable dans toutes ses dimensions (cf Fig. \ref{fig:cyclic_noise}).

\footnotetext{Les fréquences pseudo-aléatoires continues du bruit de Perlin sont calculées à partir d'une fonction de hachage sur un noyau de valeurs. Par exemple, en 2D : $Freq(x,y) = Hash(x,y) \circ Hash(x+\epsilon,y) \circ Hash(x,y+\epsilon) \circ Hash(x+\epsilon,y+\epsilon)$ pour un noyau de taille $\epsilon$. C'est ce noyau de valeurs qui induit la continuité des fréquences, et donc du bruit.}

\begin{figure*}
\centering
  \begin{subfigure}[t]{.4\textwidth}
  \centering
  \includegraphics[width=\linewidth]{pics/cyclic_noise.jpg}
  \end{subfigure}%
  ~
  \hspace{.05\textwidth}
  \begin{subfigure}[t]{.4\textwidth}
  \centering
  \includegraphics[width=\linewidth]{pics/cyclic_noise_grid.jpg}
  \end{subfigure}
  \caption{\label{fig:cyclic_noise}Bruit de Perlin additif 2D cyclique, de cycle $(1,1)$.}
\end{figure*}

\section{État de l'art : du bruit procédural aux nuages de galaxies}

Les nuages de poussière galactiques sont hétérogènes : leur densité varie d'un point à un autre. Ils apparaissent sombres sur les images des galaxies, car ils masquent la lumière des étoiles (ils agissent donc comme des volumes partiellement opaques). Afin de représenter les nuages de poussière de manière réaliste, la première étape consiste à représenter des densités dans un volume (par exemple avec un bruit procédural 3D \cite{ebert2003texturing}, ou selon un modèle plus élaboré dans le cas de nuages structurés). Ces données volumiques de densité peuvent ensuite être utilisées pour calculer le comportement de la lumière à l'intérieur du nuage \cite{bouthors2008interactive} \cite{Guillaumereport} (cela se traduit de manière simplifiée pour des nuages de poussière opaques par une fonction non linéaire qui donne une opacité à partir d'une densité). Nous obtenons ainsi l'opacité des nuages de poussière, qu'il est alors simple d'utiliser pour masquer la lumière des étoiles et ainsi obtenir leur aspect final.

Le bruit procédural seul ne respecte pas la structure des grandes échelles des nuages de poussière (organisés en filaments). La littérature énonce plusieurs méthodes permettant de créer des filaments (également appelés veines) bruités. L'idée générale est de créer le filament grâce à une fonction présentant un pic qui formera la veine, qui est ensuite combiné à un bruit procédural (par somme, produit, seuillage, etc. ou par une combinaison de ces opérations, selon l'aspect souhaité)\cite{ebert2003texturing}.

\section{Problématique : modélisation des filaments de nuages de poussière}
Nous nous intéressons dans la suite de ce rapport à la modélisation procédurale des nuages de poussière dans les galaxies spirales. Nous recherchons une modélisation des nuages sous forme de filaments suivant les bras spiraux et de \textit{spurs}. Nous voulons donc créer des veines le long des bras et des \textit{spurs} (qui forment les aspects des nuages à grande et moyenne échelle), et les combiner à un bruit de Perlin multiplicatif (qui forme la structure fractale des nuages aux petites échelles).

Nous proposons pour cela dans le Chapitre \ref{chap:contrib} une méthode de modélisation 2D des nuages de poussière galactiques. Notre objectif a été que cette méthode soit généralisable à une modélisation 3D : nous n'utilisons que des opérations généralisables physiquement et mathématiquement vers une méthode 3D.

Notre intuition est que la structure des nuages de poussière est homogène le long des bras spiraux, dans l'espace paramétré de la spirale. C'est pourquoi nous commençons par déterminer une paramétrisation de spirale (Section \ref{sec:parametrisation}), qui nous permet de mesurer les propriétés des nuages de poussière dans les images de référence (Section \ref{sec:observation}), afin de produire un modèle respectant ces propriétés (Section \ref{sec:modelisation}).